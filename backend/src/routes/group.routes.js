const { Router } = require("express");
const router = Router();
const groupCrtl = require("../controllers/group.controler")

router.get("/get-groups", groupCrtl.getGroups);

router.post("/post-group", groupCrtl.postGroup);

router.put('/put-group/:id', groupCrtl.putGroup);

router.put("/put-user", groupCrtl.putUserGroup);

router.put("/put-todo", groupCrtl.putTodoGroup);

router.delete("/delete/:id", groupCrtl.deleteGroup);

module.exports = router;