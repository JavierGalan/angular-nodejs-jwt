const { Router } = require('express');
const router = Router();
const userCrtl = require('../controllers/sign.controler');

const verifyToken = require("../auth/index.auth");



router.get('/', (req, res) => res.send('Hello world'));

router.post('/signup', userCrtl.signUp);

router.post('/signin', userCrtl.signIn);

module.exports = router;