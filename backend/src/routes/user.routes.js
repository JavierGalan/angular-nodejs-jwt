const { Router } = require("express");
const router = Router();
const userCrtl = require("../controllers/user.controler");

router.get("/users", userCrtl.getUsers);
router.get("/users/:id", userCrtl.getUser);
router.delete("/delete/:id", userCrtl.deleteUser);

module.exports = router;