const { Router } = require("express");
const router = Router();
const todoCrtl = require("../controllers/todo.controler");

router.get('/get-todos', todoCrtl.getTodos);
router.get('/get-todo/:id', todoCrtl.getTodo);
router.post('/post-todo', todoCrtl.postTodo);
router.delete('/delete/:id', todoCrtl.deleteTodo);
router.put('/put-task', todoCrtl.putTask);

module.exports = router;