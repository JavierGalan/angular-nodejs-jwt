const express = require('express');
const app = express();
const cors = require('cors');

const db = require('./config/db.config');
db.connect();

// agrega cabeceras a la peticion para poder pasarlo a este servidor, por ahora magia
app.use(cors());
// convierte los datos que recibe el servidor a u formato que podemos manipular
app.use(express.json())

app.use("/",require('./routes/index.routes'));
app.use('/group', require('./routes/group.routes'));
app.use('/todo', require('./routes/todo.routes'));
app.use("/user", require("./routes/user.routes"));

app.listen(3000);
console.log('Server on port', 3000);