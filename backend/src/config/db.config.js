const mongoose = require("mongoose");

const DB_URL = "mongodb://localhost:27017/angular-node-jwt";

const connect = async () => {
  try {
    await mongoose.connect(DB_URL, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
    });

    console.log("DB connected successfuly");
  } catch (error) {
    console.log(`Something happend trying to connect to DB: ${error}`);
  }
};

module.exports = { DB_URL, connect };
