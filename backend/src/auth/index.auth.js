const secretKey = require("./utils.auth");
const jwt = require("jsonwebtoken");

function verifyToken(req, res, next) {
  if (!req.headers.authorization) {
    return res.status(401).send("unauthorize Request");
  }
  // separa el token de bearer
  const token = req.headers.authorization.split(" ")[1];
  if (token === "null") {
    return res.status(401).send("Unauthorize Request");
  }

  //   payload contiene el id del usuario
  const payload = jwt.verify(token, secretKey);
  req.userId = payload._id;
  next();
}
module.exports = verifyToken;
