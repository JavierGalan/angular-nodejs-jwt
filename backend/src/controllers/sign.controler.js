const User = require("../models/User.model");
const userCrtl = {};

const jwt = require("jsonwebtoken");
const secretKey = require("../auth/utils.auth");


userCrtl.signUp = async (req, res) => {
  const { email, password } = req.body;
  const newUser = new User({ email, password });
  await newUser.save();

  /**
   * metodo sign tiene 3 parámetros
   * 1. el dato que queremos guardar dentro del token
   * 2. palabra secreta
   * 3. opciones (cuanto quieres que dure, si quieres validaciones...)
   */
  const token = jwt.sign({ _id: newUser._id }, secretKey);
  res.status(200).json({ token });
};

userCrtl.signIn = async (req, res) => {
  const { email, password } = req.body;
  const user = await User.findOne({ email });
  if (!user) return res.status(401).send("The email doesn't exists");
  if (user.password !== password) return res.status(401).send("Wrong Password");

  const token = jwt.sign({ _id: user._id }, secretKey);
  res.status(200).json({ token });
};

module.exports = userCrtl; 