const Group = require("../models/Group.model");
const groupCrtl = {};

groupCrtl.getGroups = async (req, res, next) => {
  try {
    const groups = await Group.find().populate("users");
    return res.status(200).json(groups);
  } catch (error) {
    return res.status(500).json(error);
  }
};

groupCrtl.getGroup = async (req, res, next) => {
  try {
    const id = req.params.id;
    const group = await Group.findById(id);
    return res.status(200).json(group);
  } catch (error) {
    next(error);
  }
};

groupCrtl.postGroup = async (req, res, next) => {
  try {
    const newGroup = new Group({
      name: req.body.name,
      description: req.body.description,
    });
    const createdGroup = await newGroup.save();
    return res.status(200).json(createdGroup);
  } catch (error) {
    next(error);
  }
};

groupCrtl.putGroup = async (req, res, next) => {
  try {
    const id = req.params.id;
    const updatedGroup = await Group.findByIdAndUpdate(
      id,
      {
        name: req.body.name,
        description: req.body.description,
      },
      { new: true }
    );
    return res.status(200).json(updatedGroup);
  } catch (error) {
    next(error);
  }
};

groupCrtl.putUserGroup = async (req, res, next) => {
  try {
    const groupId = req.body.groupId;
    const userId = req.body.userId;

    const updatedGroup = await Group.findByIdAndUpdate(
      groupId,
      { $push: { users: userId } },
      { new: true }
    );
    console.log(updatedGroup);
    return res.status(200).json(updatedGroup);
  } catch (error) {
    next(error);
  }
};

groupCrtl.putTodoGroup = async (req, res, next) => {
  try {
    const groupId = req.body.groupId;
    const todoId = req.body.todoId;

    const updatedGroup = await Group.findByIdAndUpdate(
      groupId,
      { $push: { todos: todoId } },
      { new: true }
    );
    return res.status(200).json(updatedGroup);
  } catch (error) {
    next(error);
  }
};

groupCrtl.deleteGroup = async (req, res, next) => {
  try {
    const id = req.params.id;
    await Group.findByIdAndDelete(id);
    return res.status(200).json("Group deleted");
  } catch (error) {
    next(error);
  }
};

module.exports = groupCrtl;
