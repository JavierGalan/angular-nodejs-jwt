const Todo = require("../models/Todo.model");

const todoCrtl = {};

todoCrtl.postTodo = async (req, res, next) => {
    try {
        const newTodo = new Todo({
            title: req.body.title,
        });
        const createdTodo = await newTodo.save();
        return res.status(200).json(createdTodo);
    } catch (error) {
        next(error);
    }
};

todoCrtl.getTodo = async (req, res, next) => {
    try {
        const id = req.params.id;
        const todo = await Todo.findById(id);
        return res.status(200).json(todo);
    } catch (error) {
        next(error);
    }
};

todoCrtl.getTodos = async (req, res) => {
  try {
    const todos = await Todo.find();
    return res.status(200).json(todos);
  } catch (error) {
    return res.status(500).json(error);
  }
};

todoCrtl.putTask = async (req, res, next) => {
  try {
    const todoId = req.body.todoId;
    const todoTask = req.body.todoTask;

    const updatedTodo = await Todo.findByIdAndUpdate(
      todoId,
      { $push: { tasks: `${todoTask}` } },
      { new: true }
    );
    return res.status(200).json(updatedTodo);
  } catch (error) {
    next(error);
  }
};

todoCrtl.editTitle = async (req, res, next) => {
  try {
    const id = req.params.id;
    const updatedTodo = await Todo.findByIdAndUpdate(
      id,
      {
        title: req.body.title,
      },
      { new: true }
    );
    return res.status(200).json(updatedTodo);
  } catch (error) {
    next(error);
  }
};

todoCrtl.deleteTodo = async (req, res, next) => {
  try {
    const id = req.params.id;
    await Todo.findByIdAndDelete(id);
    return res.status(200).json("Todo deleted");
  } catch (error) {
    next(error);
  }
};


// todoCrtl.postTodo = async (req, res, next) => {};

module.exports = todoCrtl;