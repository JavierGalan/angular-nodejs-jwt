const mongoose = require("mongoose");

const { Schema, model } = require("mongoose");

const groupSchema = new Schema(
  {
    name: { type: String, default: "Default group" },
    description: { type: String, default: "Sample Text" },
    todos: [{ type: mongoose.Types.ObjectId, ref: "Todo" }],
    users: [{ type: mongoose.Types.ObjectId, ref: "User" }],
  },
  {
    timestamps: true,
  }
);

const Group = model("Group", groupSchema);

module.exports = Group;
