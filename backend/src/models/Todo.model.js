const { Schema, model } = require("mongoose");
const mongoose = require("mongoose");

const todoSchema = new Schema(
  {
    title: { type: String, default: "ToDo" },
    tasks: [{ type: String }],
    group: { type: mongoose.Types.ObjectId, ref: "Group" },
  },
  {
    timestamps: true,
  }
);

const Todo = model("Todo", todoSchema);

module.exports = Todo;
