import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { Iuser } from "../models/iuser";
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private URL = environment.baseUrl;

  constructor(
    private http: HttpClient,
    private router: Router,
    ) {}

  register(user: Iuser) {
    return this.http.post(`${this.URL}/signup`, user);
  }

  login(user: Iuser) {
    return this.http.post(`${this.URL}/signin`, user);
  }

  loggedIn(){
    if (localStorage.getItem('token')) {
      return true;
    }
    return false;
  }

  getToken(){
    return localStorage.getItem('token');
  }

  logout(){
    localStorage.removeItem('token');
    this.router.navigate(['/signin']);
  }

}
