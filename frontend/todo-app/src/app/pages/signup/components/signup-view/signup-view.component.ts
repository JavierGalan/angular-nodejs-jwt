import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Iuser } from 'src/app/models/iuser';
import { AuthService } from '../../../../services/auth.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-signup-view',
  templateUrl: './signup-view.component.html',
  styleUrls: ['./signup-view.component.scss'],
})
export class SignupViewComponent implements OnInit {

  public userSignUpForm: FormGroup;

  public submitted: boolean = false;

  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private router:Router,
    ) {
    this.userSignUpForm = this.formBuilder.group({
      email: [''],
      password: [''],
    });
  }

  ngOnInit(): void {}

  signUp() {
    this.submitted = true;
    if (this.userSignUpForm.valid) {
      const user: Iuser = {
        email: this.userSignUpForm.get('email')?.value,
        password: this.userSignUpForm.get('password')?.value,
      };
      console.log(user);
      this.userSignUpForm.reset();
      this.submitted = false;

      this.authService.register(user)
        .subscribe(
          res => {
            console.log(Object.values(res));
            // no me deja acceder al res.token
            localStorage.setItem('token', Object.values(res)[0]);
            this.router.navigate(['/home']);
          },
          err => {
            console.log(err);
          }
        );

    }
  }
}
