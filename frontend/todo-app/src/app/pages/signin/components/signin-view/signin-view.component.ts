import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../../../services/auth.service';
import { Router } from "@angular/router";
import { Iuser } from 'src/app/models/iuser';

@Component({
  selector: 'app-signin-view',
  templateUrl: './signin-view.component.html',
  styleUrls: ['./signin-view.component.scss'],
})
export class SigninViewComponent implements OnInit {
  public userSignInForm: FormGroup;

  public submitted: boolean = false;

  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
    private router: Router,
  ) {
    this.userSignInForm = this.formBuilder.group({
      email: [''],
      password: [''],
    });
  }

  ngOnInit(): void {}

  signIn() {
    this.submitted = true;
    if (this.userSignInForm.valid) {
      const user: Iuser = {
        email: this.userSignInForm.get('email')?.value,
        password: this.userSignInForm.get('password')?.value,
      };
      this.userSignInForm.reset();
      this.submitted = false;

      this.authService.login(user).subscribe(
        (res) => {
          console.log(Object.values(res));
          // no me deja acceder al res.token
          localStorage.setItem('token', Object.values(res)[0]);
          this.router.navigate(['/home']);
        },
        (err) => {
          console.log(err);
        }
      );
    }
  }
}
