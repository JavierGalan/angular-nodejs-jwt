import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';"@angular/forms";

import { SigninRoutingModule } from './signin-routing.module';
import { SigninViewComponent } from './components/signin-view/signin-view.component';


@NgModule({
  declarations: [SigninViewComponent],
  imports: [
    CommonModule,
    SigninRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class SigninModule {}
