import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GroupsViewComponent } from './components/groups-view/groups-view.component';

const routes: Routes = [
  {
    path:"", component: GroupsViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GroupsRoutingModule { }
